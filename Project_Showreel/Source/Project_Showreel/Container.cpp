// Fill out your copyright notice in the Description page of Project Settings.


#include "Container.h"

// Sets default values
AContainer::AContainer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AContainer::BeginPlay()
{
	Super::BeginPlay();
	
	SpawnPosition = GetActorLocation();
	SpawnRotation = GetActorRotation();

	CurrentItem = GetWorld()->SpawnActor<AItem>(ItemsCatalogue[0], SpawnPosition, SpawnRotation);
}

// Called every frame
void AContainer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AContainer::SwitchItem(int newItemIndex)
{
	CurrentItem->Destroy();
	CurrentItem = nullptr;

	CurrentItem = GetWorld()->SpawnActor<AItem>(ItemsCatalogue[newItemIndex], SpawnPosition, SpawnRotation);
}

ASpotLight* AContainer::GetLightAt(int LightIndex) const
{
	return Lights[LightIndex];
}

