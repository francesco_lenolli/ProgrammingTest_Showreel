// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Item.h"
#include "Engine/SpotLight.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Container.generated.h"

UCLASS()
class PROJECT_SHOWREEL_API AContainer : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AContainer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Scene Mechanics")
	void SwitchItem(int newItemIndex);

	UFUNCTION(BlueprintCallable, Category = "Scene Mechanics")
	ASpotLight* GetLightAt(int LightIndex) const;

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Scene Elements")
	TArray<ASpotLight*> Lights;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Scene Elements")
	TArray<TSubclassOf<AItem>> ItemsCatalogue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Scene Elements")
	AItem* CurrentItem;

	// Lights of TArray Lights are children of this object.
	// Used to rotate Lights around Item.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Scene Elements")
	AActor* LightsPivot;

private:

	FVector SpawnPosition;
	FRotator SpawnRotation;
};
