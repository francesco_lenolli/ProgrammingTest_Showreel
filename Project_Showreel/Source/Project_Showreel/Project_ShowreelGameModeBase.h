// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Blueprint/UserWidget.h"
#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Project_ShowreelGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PROJECT_SHOWREEL_API AProject_ShowreelGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	

public:

	UFUNCTION(BlueprintCallable, Category = "User Interface")
	void ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass);

protected:

	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "User Interface")
	TSubclassOf<UUserWidget> StartingWidgetClass;

	UPROPERTY()
	UUserWidget* CurrentWidget;
};
